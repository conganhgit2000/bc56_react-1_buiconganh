import React, { Component } from "react";

export default class Item extends Component {
  render() {
    return (
      <div className="card item-card">
        <div className="card-body bg-light item-card-body">
          <a href="#" className="btn-card btn btn-primary">
            <i class="fa-brands fa-bootstrap"></i>
          </a>
          <h5 className="card-title">Fresh new layout</h5>
          <p className="card-text">
            With Bootstrap 5, we've created a fresh new layout for this
            template!
          </p>
        </div>
      </div>
    );
  }
}
