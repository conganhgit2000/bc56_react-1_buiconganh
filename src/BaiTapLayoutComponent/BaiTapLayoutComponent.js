import React, { Component } from "react";
import Header from "./Header";
import Banner from "./Banner";
import Item from "./Item";
import Footer from "./Footer";

export default class BaiTapLayoutComponent extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="container m-auto">
          <div className="row">
            <div className="col-12">
              <Banner />
            </div>
          </div>
          <div className="row">
            <div className="col-4">
              <Item />
            </div>
            <div className="col-4">
              <Item />
            </div>
            <div className="col-4">
              <Item />
            </div>
            <div className="col-4">
              <Item />
            </div>
            <div className="col-4">
              <Item />
            </div>
            <div className="col-4">
              <Item />
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
