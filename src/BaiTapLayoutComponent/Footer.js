import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div
        style={{ backgroundColor: "rgb(33, 37, 41)" }}
        className="text-light py-5 mt-4"
      >
        Copyright © Your Website 2023
      </div>
    );
  }
}
