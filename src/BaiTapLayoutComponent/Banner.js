import React, { Component } from "react";

export default class Banner extends Component {
  render() {
    return (
      <div>
        <div className="card container banner-card">
          <div className="card-body bg-light banner-card-body">
            <h5 className="card-title">A warm welcome!</h5>
            <p className="card-text">
              Bootstrap utility classes are used to create this jumbotron since
              the old component has been removed from the framework. Why create
              custom CSS when you can use utilities?
            </p>
            <a href="#" className="btn-card btn btn-primary">
              Call to action
            </a>
          </div>
        </div>
      </div>
    );
  }
}
